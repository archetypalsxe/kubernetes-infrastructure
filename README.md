# kubernetes-infrastructure

Research project to work with:
* Kubernetes
  * AWS EKS
  * Kustomize
  * Minikube
* Terraform
  * Terragrunt
  * Localstack
  * Terratest
* Jaeger
* Elasticsearch
* Kafka
* Prometheus

## Terraform
See the [README in the Terraform directory](terraform/README.md)

## CI/CD Pipeline Setup
* Run the [#terraform)
* GitLab CI/CD Variables
  * Located in: Repository settings > CI/CD > Variables
  * Access Keys
    * A secret should be created called `cicd_user_keys`
      * Get the access key ID and access key from this secret
      * Store in as `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`
  * ECR Repository URL:
    * Should be outputted from the Terraform as `apollo_ecr_repository_url`
    * Store as `APOLLO_ECR_URL`
  * `AWS_DEFAULT_REGION` if not deploying in `us-east-1`

## Kubernetes

### Setting Up kubectl
* Make sure you have created the EKS cluster in Terraform
  * The Terraform should also set your kube context, but in case it doesn't:
    * `aws eks update-kubeconfig --name ${eks_cluster:?} --alias ${desired_name}`
* (Optional) Rename the context (if you didn't use alias in the last step)
  * `kubectl config rename-context ${old_context:?} ${new_context:?}`

## Running Locally
### Docker-Compose
* From root directory
  * `docker-compose up --build`

### Docker
* From the `apollo` Directory
* Building
    * `docker build . -t apollo`
* Running
    * Foreground
        * `docker run -p 3000:3000 apollo:latest`
    * Background
        * `docker run -p 3000:3000 -d apollo:latest`
* Accessing
  * Apollo: `localhost:3000/graphql`
  * Database Admin: `localhost:8001`

## Apollo / GraphQL

Example query to add a country:
```
mutation {
    addCountry(
        data: {
          name: "United States",
          population: 321774,
          area: 9833517,
          threatenedSpecies: 1514,
          renewableElectricityProductionPercent: 0,
          energyUseIntensity: 134,
          municipalWaste: 227604,
          recycledMunicipalWastePercent: 26,
          gdpPerCapita: 54306
        }
    ) {
        error
    }
}
```
And another:
```
mutation {
    addCountry(
        data: {
          name: "India",
          population: 1311051,
          area: 3287263,
          threatenedSpecies: 1052,
          renewableElectricityProductionPercent: 0,
          energyUseIntensity: 128,
          municipalWaste: 17569,
          recycledMunicipalWastePercent: 0,
          gdpPerCapita: 1586
        }
    ) {
        error
    }
}
```
Getting entire table:
```
query {
  countries {
    name
    population
    area
    threatenedSpecies
    renewableElectricityProductionPercent
    energyUseIntensity
    municipalWaste
    recycledMunicipalWastePercent
    gdpPerCapita
  }
}
```
Getting specific entry:
```
query {
  country(name: "India") {
    name
    population
    area
    threatenedSpecies
    renewableElectricityProductionPercent
    energyUseIntensity
    municipalWaste
    recycledMunicipalWastePercent
    gdpPerCapita
  }
}
```

### DynamoDB

#### Testing Local Setup
* `aws dynamodb list-tables --endpoint-url http://localhost:8000`