import AWS from 'aws-sdk';

import tables from './tables';

export default class Database {
    private _connection: any;

    async connect() {
        if (!this._connection) {
            let params = {};
            if (process.env.LOCAL) {
                params = {
                    endpoint: process.env.DB_URL,
                    region: process.env.REGION,
                    accessKeyId: 'local',
                    secretAccessKey: 'local',
                };
            } else {
                params = {
                    region: process.env.REGION,
                    apiVersion: '2012-08-10',
                };
            }

            this._connection = new AWS.DynamoDB(params);

            await this.createTables(tables);
        }

        return this._connection;
    }

    async putItem(params: any) {
        return new Promise((resolve, reject) => {
            this._connection.putItem(params, (err: any, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    async getItem(params: any) {
        return new Promise((resolve, reject) => {
            this._connection.getItem(params, (err: any, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    async updateItem(params: any) {
        return new Promise((resolve, reject) => {
            this._connection.updateItem(params, (err: any, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    async scan(params: any = {}) {
        return new Promise((resolve, reject) => {
            this._connection.scan(params, (err: any, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    async deleteItem(params: any) {
        return new Promise((resolve, reject) => {
            this._connection.deleteItem(params, (err: any, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    async createTables(tables: any) {
        for (let counter = 0; counter < tables.length; counter++) {
            const table = tables[counter];

            await new Promise<void>((resolve, reject) => {
                this._connection.createTable(table, (err: { code: string; }) => {
                    if (err) {
                        if (err.code !== 'ResourceInUseException') {
                            console.dir(err);
                            reject(err);
                        } else {
                            resolve();
                        }
                    } else {
                        resolve();
                    }
                });
            });
        }
    }
}