export default {
    Query: {
        countries: async (source: any, args: any, { dataSources }: any, state: any) => {
            const result = await dataSources.countrySource.getAll(null);
            if (result) {
                return result;
            }

            return null;
        },

        country: async (source: any, args: any, { dataSources }: any, state: any) => {
            const { name } = args;
            const result = await dataSources.countrySource.get([name]);
            if (result) {
                return result;
            }

            return null;
        },
    },

    Mutation: {
        addCountry: async (source: any, args: any, { dataSources }: any, state: any) => {
            const { data } = args;

            let result;
            try {
                result = await dataSources.countrySource.put(data);
            } catch (e) {
                console.error(e);
                result = 'Internal error';
            }

            return result;
        },
        deleteCountry: async (source: any, args: any, { dataSources }: any, state: any) => {
            const { name } = args;

            let result;
            try {
                await dataSources.CountrySource.delete(name);
            } catch (e) {
                console.error(e);
                result = 'Internal error';
            }

            return result;
        },
    },
};