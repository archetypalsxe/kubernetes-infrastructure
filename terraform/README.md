# Terraform - Kubernetes Infrastructure

**All commands should be ran from this (`terraform`) directory unless otherwise stated**

## Initialization
* Copy the `terragrunt-config.yaml-example` file to `terragrunt-config.yaml`
  * Modify necessary fields in this file
* `terragrunt init`

## Running
* `terragrunt plan`
* `terragrunt apply`

## Formatting / Linting
* `terraform fmt -check --diff -recursive`