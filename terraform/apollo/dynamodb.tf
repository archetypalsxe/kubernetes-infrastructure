resource "aws_dynamodb_table" "countries_table" {
  name           = "${var.project_name}-countries"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "name"

  attribute {
    name = "name"
    type = "S"
  }
}