resource "aws_ecr_repository" "apollo_ecr" {
  name         = "${var.project_name}-apollo"
  force_delete = true
  image_scanning_configuration {
    scan_on_push = true
  }
}