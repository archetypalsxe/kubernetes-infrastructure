variable "project_name" {
  description = "The name of the project to use as a prefix"
  type        = string
}