output "service_account" {
  value = kubernetes_service_account.service_account
}

output "service_account_name" {
  value = kubernetes_service_account.service_account.metadata[0].name
}