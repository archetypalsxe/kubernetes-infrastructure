variable "aws_account_id" {
  description = "The ID of the AWS account"
  type        = string
}

variable "project_name" {
  description = "The name of the project to use as a prefix"
  type        = string
}

variable "name" {
  description = "The name of the Kubernetes service account"
  type        = string
}

variable "namespace" {
  description = "The K8s namespace that the account should be created in"
  type        = string
}

variable "app_name" {
  description = "The K8s app name that should be used for this service account"
  type        = string
}

variable "eks_cluster_issuer" {
  description = "The issuer for the EKS cluster"
  type        = string
}

variable "iam_policy_arns" {
  description = "(Optional) List of IAM policy ARNs that should be attached to the created IAM role"
  type        = list(string)
  default     = []
}