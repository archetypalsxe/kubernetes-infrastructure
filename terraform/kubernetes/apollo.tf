locals {
  apollo_deployment_name = "apollo-deployment"
  apollo_app_name        = "apollo"
}

data "aws_caller_identity" "current" {}

resource "kubernetes_namespace" "apollo_namespace" {
  metadata {
    annotations = {
      name = "apollo"
    }

    labels = {
      namespace = "apollo"
    }

    name = "apollo"
  }
}

module "apollo_service_account" {
  source             = "../k8s-service-account"
  aws_account_id     = data.aws_caller_identity.current.account_id
  project_name       = var.project_name
  name               = "apollo-service-account"
  namespace          = kubernetes_namespace.apollo_namespace.id
  app_name           = local.apollo_app_name
  eks_cluster_issuer = aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer
  iam_policy_arns    = ["arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"]
}

resource "kubernetes_deployment" "apollo_deployment" {
  metadata {
    name      = local.apollo_deployment_name
    namespace = kubernetes_namespace.apollo_namespace.id
    labels = {
      app = local.apollo_app_name
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = local.apollo_app_name
      }
    }

    template {
      metadata {
        name = "apollo"
        labels = {
          app = local.apollo_app_name
        }
      }

      spec {
        service_account_name = module.apollo_service_account.service_account_name

        container {
          image = "${var.apollo_ecr_repository_url}:${var.apollo_docker_tag}"
          name  = "apollo"

          env {
            name  = "REGION"
            value = var.aws_region
          }

          port {
            container_port = 3000
          }

          liveness_probe {
            http_get {
              path = "/.well-known/apollo/server-health"
              port = 3000
            }

            period_seconds = 120
          }
        }

        toleration {
          key      = "eks.amazonaws.com/compute-type"
          value    = "fargate"
          operator = "Equal"
          effect   = "NoSchedule"
        }

      }
    }
  }
}

// #TODO (Ticketed) Temporarily removing, not working and causing edits on every Terraform run
/*
resource "kubernetes_horizontal_pod_autoscaler_v2beta2" "apollo_scaler" {
  metadata {
    name      = "apollo-scaler"
    namespace = kubernetes_namespace.apollo_namespace.id
  }

  spec {
    min_replicas = 1
    max_replicas = 5

    scale_target_ref {
      kind = "Deployment"
      name = local.apollo_deployment_name
    }

    metric {
      type = "External"
      external {
        metric {
          name = "latency"
          selector {
            match_labels = {
              app = kubernetes_deployment.apollo_deployment.metadata.0.labels.app
            }
          }
        }
        target {
          type  = "Value"
          value = "100"
        }
      }
    }
  }
}
*/

resource "kubernetes_service_v1" "apollo_service" {
  metadata {
    name      = "apollo-service"
    namespace = kubernetes_namespace.apollo_namespace.id
  }

  spec {
    selector = {
      app = kubernetes_deployment.apollo_deployment.metadata.0.labels.app
    }
    session_affinity = "ClientIP"
    port {
      port        = 3000
      target_port = 3000
      protocol    = "TCP"
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress_v1" "apollo_ingress" {
  metadata {
    name      = "apollo-ingress"
    namespace = kubernetes_namespace.apollo_namespace.id
    annotations = {
      "kubernetes.io/ingress.class"                            = "alb"
      "alb.ingress.kubernetes.io/scheme"                       = "internet-facing"
      "alb.ingress.kubernetes.io/target-type"                  = "ip"
      "alb.ingress.kubernetes.io/healthcheck-path"             = "/.well-known/apollo/server-health"
      "alb.ingress.kubernetes.io/healthcheck-interval-seconds" = "120"
      "alb.ingress.kubernetes.io/certificate-arn"              = var.apollo_certificate_arn
      "alb.ingress.kubernetes.io/listen-ports"                 = "[{\"HTTP\": 80}, {\"HTTPS\": 443}]"
      "alb.ingress.kubernetes.io/actions.ssl-redirect"         = "'{\"Type\": \"redirect\", \"RedirectConfig\": { \"Protocol\": \"HTTPS\", \"Port\": \"443\", \"StatusCode\": \"HTTP_301\"}}"
      "external-dns.alpha.kubernetes.io/hostname"              = var.apollo_domain_name
    }
  }

  spec {
    default_backend {
      service {
        name = kubernetes_service_v1.apollo_service.metadata[0].name
        port {
          number = 3000
        }
      }
    }

    rule {
      http {
        path {
          path = "/*"
          backend {
            service {
              name = kubernetes_service_v1.apollo_service.metadata[0].name
              port {
                number = 3000
              }
            }
          }
        }
      }
    }
  }
}