locals {
  private_subnet_ids = [
    for subnet in var.private_subnets : subnet.id
  ]

  public_subnet_ids = [
    for subnet in var.public_subnets : subnet.id
  ]
}

resource "aws_eks_cluster" "eks_cluster" {
  name     = var.eks_cluster_name
  role_arn = var.eks_role_arn
  version  = "1.23"

  vpc_config {
    subnet_ids = concat(local.private_subnet_ids, local.public_subnet_ids)
  }
}

# By default.... EKS is provisioned with the CoreDNS deployment expecting to be deployed
# on EC2 instances... as of the time of me coding this, you have to patch the deployment
# in order to get everything to run purely on Fargate (without a dedicated server)
# https://docs.aws.amazon.com/eks/latest/userguide/fargate-getting-started.html#fargate-gs-coredns

# Note that local-exec only runs when the resource is initially created, and never again.
# Made this its own resource so that we wouldn't have to destroy and recreate the EKS cluster
# everytime we wanted to run this
# https://www.terraform.io/language/resources/provisioners/local-exec

# Figured that the local-exec was better than importing the core-dns into a Terraform/Kubernetes
# resource and trying to keep the template/spec up-to-date manually

# I also wish that I didn't have to run the `update-kubeconfig` command here, but otherwise the
# command fails because it's impossible to have a kube context setup for a cluster that just
# finished being created
resource "null_resource" "core_dns_patcher" {
  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name ${aws_eks_cluster.eks_cluster.id} --alias ${var.project_name} && kubectl patch deployment coredns -n kube-system --type json -p='[{\"op\": \"remove\", \"path\": \"/spec/template/metadata/annotations/eks.amazonaws.com~1compute-type\"}]' && kubectl rollout restart -n kube-system deployment coredns"
  }

  depends_on = [
    time_sleep.wait_for_pods_to_populate,
    aws_eks_cluster.eks_cluster,
    aws_eks_fargate_profile.fargate_profile,
  ]
}

resource "time_sleep" "wait_for_pods_to_populate" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_fargate_profile.fargate_profile,
  ]
  create_duration = "2m"
}

resource "aws_eks_fargate_profile" "fargate_profile" {
  cluster_name           = aws_eks_cluster.eks_cluster.name
  fargate_profile_name   = "${var.project_name}-default"
  pod_execution_role_arn = var.fargate_role_arn
  subnet_ids             = local.private_subnet_ids

  selector {
    namespace = "default"
  }

  selector {
    namespace = "apollo"
  }

  selector {
    namespace = "kube-system"
  }
}

# TODO CloudWatch Group