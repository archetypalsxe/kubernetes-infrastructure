output "eks_cluster" {
  value = aws_eks_cluster.eks_cluster
}

output "apollo_dns_name" {
  value = kubernetes_ingress_v1.apollo_ingress.status[0].load_balancer[0].ingress[0].hostname
}