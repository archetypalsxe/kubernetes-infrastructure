variable "project_name" {
  description = "The name of the project to use as a prefix"
  type        = string
}

variable "eks_role_arn" {
  description = "The ARN of the role to be used with the EKS cluster"
  type        = string
}

variable "fargate_role_arn" {
  description = "The ARN of the role to be used with Fargate"
  type        = string
}

variable "public_subnets" {
  description = "The IDs of the public subnets to use for the EKS cluster"
  type        = list(any)
}

variable "private_subnets" {
  description = "The IDs of the private subnets to use for the EKS cluster"
  type        = list(any)
}

variable "apollo_ecr_repository_url" {
  description = "The URL of the Apollo ECR repository"
  type        = string
}

variable "apollo_docker_tag" {
  description = "The tag for the Apollo ECR docker image that should be deployed"
  type        = string
  default     = "production"
}

variable "eks_cluster_name" {
  description = "The name of the EKS cluster"
  type        = string
}

variable "apollo_certificate_arn" {
  description = "The ARN of the Apollo ACM Certificate"
  type        = string
}

variable "apollo_domain_name" {
  description = "The domain name that we're using for Apollo"
  type        = string
}

variable "aws_region" {
  description = "The deploy target region in AWS"
  type        = string
}