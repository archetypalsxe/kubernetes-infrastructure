locals {
  apollo_domain_name                = "${var.project_name}-apollo.${var.domain_name}"
  external_dns_service_account_name = "external-dns-role"
  external_dns_service_account_ns   = "kube-system"
  external_dns_app_name             = "external-dns"
}

resource "aws_route53_zone" "apollo" {
  name = local.apollo_domain_name
}

resource "aws_acm_certificate" "apollo_ssl_cert" {
  domain_name       = local.apollo_domain_name
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "apollo_cert_validation" {
  for_each = {
    for dvo in aws_acm_certificate.apollo_ssl_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.apollo.zone_id
}

resource "aws_acm_certificate_validation" "apollo_cert_validation" {
  certificate_arn         = aws_acm_certificate.apollo_ssl_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.apollo_cert_validation : record.fqdn]

  timeouts {
    create = "30m"
  }
}

resource "helm_release" "external_dns" {
  name      = "external-dns-controller"
  namespace = "kube-system"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "external-dns"

  set {
    name  = "aws.zoneType"
    value = "public"
  }

  set {
    name  = "domainFilters[0]"
    value = local.apollo_domain_name
  }

  set {
    name  = "serviceAccount.name"
    value = module.external_dns_k8s_account.service_account_name
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }
}

module "external_dns_k8s_account" {
  source             = "../k8s-service-account"
  aws_account_id     = data.aws_caller_identity.current.account_id
  project_name       = var.project_name
  name               = local.external_dns_service_account_name
  namespace          = local.external_dns_service_account_ns
  app_name           = local.external_dns_app_name
  eks_cluster_issuer = var.eks_cluster.identity[0].oidc[0].issuer
  iam_policy_arns    = ["arn:aws:iam::aws:policy/AmazonRoute53FullAccess"]
}