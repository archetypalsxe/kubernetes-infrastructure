variable "project_name" {
  description = "The name of the project to use as a prefix"
  type        = string
}

variable "aws_region" {
  description = "The deploy target region in AWS"
  type        = string
}

variable "num_availability_zones" {
  description = "The number of availability zones in the given region"
  type        = number
  default     = 6 // us-east-1
}

variable "eks_cluster" {
  description = "The EKS cluster resource"
}

/*
  This can be derived from the eks_cluster, however if we rely on the
  eks_cluster when creating the subnets, we get a Terraform cycling error
  due to circular dependencies
*/
variable "eks_cluster_name" {
  description = "The name of the EKS cluster"
  type        = string
}

variable "public_subnet_count" {
  description = "The number of public subnets to create"
  type        = number
  default     = 2
}

variable "private_subnet_count" {
  description = "The number of private subnets to create"
  type        = number
  default     = 1
}

variable "domain_name" {
  description = "The name of the domain that we should deploy this to"
  type        = string
}