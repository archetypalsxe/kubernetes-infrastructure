output "apollo_ecr_repository_url" {
  value = module.apollo.apollo_ecr_repository.repository_url
}

output "apollo_dns_name" {
  value = module.kubernetes.apollo_dns_name
}