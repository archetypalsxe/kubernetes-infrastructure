locals {
  cicd_user_keys = {
    "${aws_iam_access_key.cicd_keys.id}" = "${aws_iam_access_key.cicd_keys.secret}"
  }
}

resource "aws_iam_role" "eks_role" {
  name = "${var.project_name}-eks-main"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = [
          "eks.amazonaws.com",
          "eks-fargate-pods.amazonaws.com"
        ]
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_role.name
}

resource "aws_iam_role_policy_attachment" "eks_security_groups_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eks_role.name
}

resource "aws_iam_role" "fargate_role" {
  name = "${var.project_name}-fargate"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = [
          "eks.amazonaws.com",
          "eks-fargate-pods.amazonaws.com"
        ]
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "fargate_pod_execution_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = aws_iam_role.fargate_role.name
}

resource "aws_iam_role_policy_attachment" "fargate_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.fargate_role.name
}

resource "aws_iam_role_policy_attachment" "fargate_vpc_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.fargate_role.name
}

resource "aws_iam_user" "cicd_user" {
  name = "${var.project_name}-cicd"
  path = "/"
}

resource "aws_iam_access_key" "cicd_keys" {
  user   = aws_iam_user.cicd_user.name
  status = "Active"
}

data "aws_iam_policy_document" "ecr_access" {
  statement {
    sid = "EcrAuthorization"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "ecr_access" {
  name   = "${var.project_name}-ecr-access"
  policy = data.aws_iam_policy_document.ecr_access.json
}

resource "aws_iam_user_policy_attachment" "ecr_access" {
  policy_arn = aws_iam_policy.ecr_access.arn
  user       = aws_iam_user.cicd_user.name
}

data "aws_iam_policy_document" "ecr_push_access" {
  statement {
    sid = "EcrAuthorization"
    actions = [
      "ecr:CompleteLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:InitiateLayerUpload",
      "ecr:BatchCheckLayerAvailability",
      "ecr:PutImage",
    ]
    resources = [
      var.ecr_repository_arn,
    ]
  }
}

resource "aws_iam_policy" "ecr_push_access" {
  name   = "${var.project_name}-ecr-push-access"
  policy = data.aws_iam_policy_document.ecr_push_access.json
}

resource "aws_iam_user_policy_attachment" "cicd_ecr_push_access" {
  policy_arn = aws_iam_policy.ecr_push_access.arn
  user       = aws_iam_user.cicd_user.name
}