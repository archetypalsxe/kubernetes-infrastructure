output "eks_role" {
  value = aws_iam_role.eks_role
}

output "fargate_role" {
  value = aws_iam_role.fargate_role
}