resource "aws_secretsmanager_secret" "cicd_user_keys" {
  name = "cicd_user_keys"
}

resource "aws_secretsmanager_secret_version" "cicd_user_key_value" {
  secret_id     = aws_secretsmanager_secret.cicd_user_keys.id
  secret_string = jsonencode(local.cicd_user_keys)
}