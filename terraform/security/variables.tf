variable "project_name" {
  description = "The name of the project to use as a prefix"
  type        = string
}

variable "ecr_repository_arn" {
  description = "The ARN of the ECR repository that we want the CI/CD user to be able to deploy to"
  type        = string
}