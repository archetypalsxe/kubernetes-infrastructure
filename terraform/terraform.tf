locals {
  eks_cluster_name = "${var.project_name}-eks-cluster"
}

module "networking" {
  source           = "./networking"
  project_name     = var.project_name
  aws_region       = var.aws_region
  eks_cluster      = module.kubernetes.eks_cluster
  eks_cluster_name = local.eks_cluster_name
  domain_name      = var.domain_name
}

module "security" {
  source             = "./security"
  project_name       = var.project_name
  ecr_repository_arn = module.apollo.apollo_ecr_repository.arn
}

module "kubernetes" {
  source                    = "./kubernetes"
  aws_region                = var.aws_region
  project_name              = var.project_name
  public_subnets            = module.networking.public_subnets
  private_subnets           = module.networking.private_subnets
  eks_role_arn              = module.security.eks_role.arn
  fargate_role_arn          = module.security.fargate_role.arn
  apollo_ecr_repository_url = module.apollo.apollo_ecr_repository.repository_url
  eks_cluster_name          = local.eks_cluster_name
  apollo_certificate_arn    = module.networking.apollo_acm_cert.arn
  apollo_domain_name        = module.networking.apollo_acm_cert.domain_name
}

module "apollo" {
  source       = "./apollo"
  project_name = var.project_name
}