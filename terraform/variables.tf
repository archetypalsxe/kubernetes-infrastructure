variable "aws_region" {
  description = "The deploy target region in AWS"
  type        = string
}

variable "project_name" {
  description = "The name of the project to use as a prefix"
  type        = string
}

variable "domain_name" {
  description = "The name of the domain that we should use"
  type        = string
}